import React, { Component } from 'react';
import Header from './components/Header';
import UserForm from './components/UserForm';
import Error from './components/Error';
import Weather from './components/Weather';

class App extends Component {

  state = {
    error: '',
    request: {},
    response: {}
  }

  componentDidMount() {
    this.setState({
      error: false
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.request !== this.state.request) {
      this.getFromAPI();
    }

  }

  getFromAPI = () => {
    const {city, country} = this.state.request;
    if(!city || !country) return null;

    //Read URL and add API Key
    const key = '36d57ef120a6c8e65cbd6b144ae3fb93';
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${key}`;

    //Query
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({
          response: data
        });
      })
      .catch(error => {
        console.log(error);
      })

    //Get with fetch 

  }
  
  dataChecked = (response) => {
    //Validate fields
    if(response.city === '' || response.country === '') {
      this.setState({
        error: true
      })
    } else {
      this.setState({
        request: response,
        error: false
      });
    }
  }

  render() {
    const { error } = this.state;
    const { cod } = this.state.response;

    let result;
    if (error) {
      result = <Error message="All fields must be filled!" />
    } else if (cod === '404') {
      result = <Error message="City not found. Check spelling." />
    } else {
      result = <Weather response={this.state.response}/>
    }

    return (
      <div className="App">
        <Header 
          title="Check the weather!"
        />
        <UserForm
          dataChecked={this.dataChecked}
        />
        {result}
      </div>
    );
  }
}

export default App;
