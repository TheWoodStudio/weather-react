import React, { Component } from 'react';
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Select from 'muicss/lib/react/select';
import Option from 'muicss/lib/react/option';
import Button from 'muicss/lib/react/button';
import PropTypes from 'prop-types';

class userForm extends Component {

    getWeather = (e) => {
        e.preventDefault();
        
        //Get refs and create Obj
        const cityRef = this.cityRef.controlEl.value;
        const countryRef = this.countryRef.controlEl.value;

        const response = {
            city: cityRef,
            country: countryRef
        }

        //Send to main component by props
        this.props.dataChecked(response);
    }
    
    render() { 
        return ( 
            <Form onSubmit={this.getWeather}>
                <Input label="City" floatingLabel={true} ref={input => {this.cityRef = input;}}/>
                <Select name="" ref={el => { this.countryRef = el; }}>
                    <Option value="" label="Country" />                    
                    <Option value="AR" label="Argentina" />
                    <Option value="BR" label="Brazil" />
                    <Option value="CL" label="Chile" />
                    <Option value="CO" label="Colombia" />
                    <Option value="PE" label="Peru" />
                    <Option value="US" label="United States" />
                </Select>
                <Button type="submit" color="primary">Weather!</Button>
            </Form>
         );
    }
}
 
userForm.propTypes = {
    dataChecked: PropTypes.func.isRequired
}

export default userForm;