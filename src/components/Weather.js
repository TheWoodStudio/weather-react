import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Weather extends Component {
    showResults = () => {
        //Get request data
        const { name, weather, main } = this.props.response;
        if (!name || !weather || !main) return null;

        const kelvin = 273.15;
        const iconURL = `http://openweathermap.org/img/w/${weather[0].icon}.png`;
        const imgAlt = `Weather in ${name}`;

        return (
            <div className="response">
                <h3>{name}</h3>
                <div className="temperature">
                    <p>{(main.temp - kelvin).toFixed(1)}&deg;C</p>
                    <img src={iconURL} alt={imgAlt} />
                </div>
                <p>Min: {(main.temp_min - kelvin).toFixed(1)}&deg;C</p>
                <p>Max: {(main.temp_max - kelvin).toFixed(1)}&deg;C</p>
            </div>
        );
    }
    
    render() { 
        return ( 
            <div>
                {this.showResults()}
            </div>
         );
    }
}
 
Weather.propTypes = {
    response: PropTypes.object.isRequired,
     
}

export default Weather;